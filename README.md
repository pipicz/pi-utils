[![Coverage Status](https://coveralls.io/repos/bitbucket/pipicz/pi-utils/badge.svg?branch=master)](https://coveralls.io/bitbucket/pipicz/pi-utils?branch=master)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/caa72a76ac42496da705599664a0f9ef)](https://www.codacy.com/app/istvan-pipicz/pi-utils?utm_source=pipicz@bitbucket.org&amp;utm_medium=referral&amp;utm_content=pipicz/pi-utils&amp;utm_campaign=Badge_Grade)

# piUtils

### Contains the following classes:
- ArrayUtils
- ListMap
- SetMap
